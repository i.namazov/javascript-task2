Theoretical question

1 Describe in your own words what the functions in programming are for.
As we know tha JavaScript provides supply like to most of the programming languages.
At the same time, in JavaScript, a function use to you to define a block of code, give it a name and then execute it as
many times as you want. JavaScript function can be defined using function keyword.

function HelloWorld() {
    alert("Hello World!");
}
ShowMessage();


Function Parameters

Till now we have heard a lot about function parameters but haven\’t discussed them in details. Parameters are additional
information passed to a function. The parameters are passed to the function within parentheses after the function name
and separated by commas. A function in JavaScript can have any number of parameters and also at the same time a function
in JavaScript can not have a single parameter.

Return Statement:

There are some situations when we want to return some values from a function after performing some
operations. In such cases, we can make use of the return statement in JavaScript. This is an optional statement and most
of the times the last statement in a JavaScript function.



2 Describe in your own words what is the reason why arguments are defined in a function. Why are they passed when a function is called?

Because JavaScipt functions, it is better to collect all of the arguments. If we collect all the arguments in 1 function
and then call this function, all arguments will be passed automatically.By returning a function at the end of the
function, we hold the function result ready for transmission. After closing the function, all the arguments will pass
if we call to run the function. I want to show an example from Task2.

function operater (number1,number2,operation) {
    let result;
    switch (operation) {
        case "+":
        result = number1+number2;
        break;
        case "-":
            result = number1-number2;
            break;
        case "*":
            result = number1*number2;
            break;
        case "/":
            result = number1/number2;
            break;
    }             return result;

}
console.log(operater(number1,number2,operation));
